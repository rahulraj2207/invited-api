import 'dart:convert';
import 'package:attending/models/invitedData.dart';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:attending/services/invitedApi.dart';
var data = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIwMDU0Mjc5LCJqdGkiOiI5ZGJhMjhiZjMwZjE0ZTNkOGFmNTY4NjJjN2MzM2M5YyIsInVzZXJfaWQiOjR9.g5pEMSAEHabB_bNOx1WpLSHxLYyiouHvcSBo2LxJhNY";
class InvitedController extends GetxController {
  RxBool isLoading = true.obs;
  RxList<InvitedData> invitedData = <InvitedData>[].obs;
  // var invitedData = List<InvitedData>().obs;
  @override
  void onInit() {
    print('controller');
    invitedFetchApi(12);
    super.onInit();
  }
  void invitedFetchApi(eventid) async {
    try {
      print('controllerfetch');

      isLoading(true);
      await InvitedApi().fetchApi(token: data, event:eventid).then((response) {
        if (response.statusCode == 200) {
          var events = invitedDataFromJson(response.body);
          invitedData.addAll(events);
        }else {
          print('error occured in events details Page');
        }
        });
    }finally{
      isLoading(false);
    }
  }
}

