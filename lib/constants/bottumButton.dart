import 'package:flutter/material.dart';
import 'colors.dart';

class BottomButton extends StatelessWidget {
  String inputText;
  BottomButton(this.inputText);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: RaisedButton(
          color: ColorConstant.senderBubbleChat,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(
            inputText,
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.w700),
          ),
          onPressed: () {},
        ),
      ),
    );
  }
}
