import 'package:flutter/material.dart';

class IconConstants {
  static Icon backIcon = Icon(
    Icons.arrow_back_ios,
    color: Colors.black54,
  );
}
