import 'package:flutter/material.dart';

class UserChatIcon extends StatelessWidget {
  const UserChatIcon({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 30,
      height: 30,
      decoration: BoxDecoration(
        color: Colors.red,
        shape: BoxShape.circle,
        image: DecorationImage(
            image: AssetImage('assets/images/sarah.jpg'), fit: BoxFit.fill),
      ),
    );
  }
}
