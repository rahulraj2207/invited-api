import 'package:flutter/material.dart';

class TextConstants {
  static TextStyle appBarText =
      TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.w900);
  static TextStyle chatBubbleText = TextStyle(fontSize: 16.0);

  static TextStyle actionSheetText = TextStyle(
    color: Colors.black,
  );
}
