import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:attending/controllers/invitedController.dart';
class FollowTile extends StatelessWidget {
  // final People people;
  // FollowTile({this.people});

  @override
  Widget build(BuildContext context) {
       return ListTile(
          leading: CircleAvatar(
            radius: 25.0,
            // backgroundImage: NetworkImage(people.dp),
            backgroundColor: Colors.transparent,
          ),
          title: Text(
            "kijk",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text("lkk"),
          trailing: SelectorButton(
            id: 1,
            isActive: !false,
            request: true,
          ));

  }
}

class SelectorButton extends StatelessWidget {
  @required
  final bool isActive;
  final int id;
  final bool request;

  SelectorButton({required this.isActive, required this.id, required this.request});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: GestureDetector(
        child: Container(
            width: MediaQuery.of(context).size.width * 0.3,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
            child: Text(
                request
                    ? "REQUESTED"
                    : isActive
                    ? "FOLLOW"
                    : "FOLLOWING",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 13,
                    color: request
                        ? Colors.black54
                        : isActive
                        ? Colors.white
                        : Colors.black54,
                    fontWeight: FontWeight.bold)),
            decoration: BoxDecoration(
                border: request
                    ? Border.all(color: Colors.grey.shade300)
                    : isActive
                    ? null
                    : Border.all(color: Colors.grey.shade300),
                borderRadius: BorderRadius.circular(14.0),
                color: request
                    ? null
                    : isActive
                    ? Colors.red
                    : null)),
      ),
    );
  }
}
