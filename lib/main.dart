import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:attending/routes.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Navigation Basics',
      home: FirstRoute(),
    );
  }
}
