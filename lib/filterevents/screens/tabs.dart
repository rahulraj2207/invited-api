import 'package:attending/controllers/invitedController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:attending/constants/colors.dart';
import 'package:attending/constants/icons.dart';
import 'package:attending/constants/textstyle.dart';
import 'package:attending/routes.dart';
import 'package:attending/views/followTile.dart';

class Tabs extends StatefulWidget {


  @override
  _TabsState createState() => _TabsState();
}



class _TabsState extends State<Tabs> {

  final InvitedController invitedController=Get.put(InvitedController());


  @override
  void initState() {
    invitedController.invitedFetchApi(12);
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          bottom: TabBar(
            indicatorColor: ColorConstant.senderBubbleChat,
            indicatorSize: TabBarIndicatorSize.tab,
            unselectedLabelColor: Colors.black54,
            labelColor: Colors.black,
            tabs: [
              TabText('Invited'),
              TabText('Attending'),
              TabText('Maybe'),
              TabText("can't go"),
            ],
          ),
          leading: IconButton(
            padding: EdgeInsets.only(left: 5),
            icon: IconConstants.backIcon,
            iconSize: 20,
            color: Colors.black54,
            onPressed: () {
              Get.to(FirstRoute());
            },
          ),
          title: Text(
            'Scottish Lake',
            style: TextConstants.appBarText,
          ),
          centerTitle: true,
          elevation: 0,
        ),
        body: TabBarView(
          children: [
            noUserFound(),
            FollowTile(),


            ListView.builder(
                itemCount: 4,
                itemBuilder: (context, i) {


                  return Text("Data1");
                }),
            Text(''),

          ],
        ),
      ),
    );
  }

  Padding TabText(String inputText) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Text(
        inputText,
        style: TextStyle(fontSize: 13, fontWeight: FontWeight.w800),
      ),
    );
  }
}



class noUserFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          radius: 45,
          backgroundColor: Color(0xfffbf4eb),
          child: Icon(
            Icons.do_not_disturb,
            color: Colors.deepOrange,
            size: 35,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
          child: Text(
            'No User Found',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w900,
                fontSize: 25),
          ),
        ),
        Text(
          "Ahh...It seems that any event doesn't\ntakes place today.We're really sorry for ",
          style: TextStyle(
              color: Colors.black45,
              fontWeight: FontWeight.w900,
              fontSize: 15),
        ),
        Text(
          'that',
          style: TextStyle(
              color: Colors.black45,
              fontWeight: FontWeight.w900,
              fontSize: 15),
        )
      ],
    );
  }
}
