// To parse this JSON data, do
//
//     final invitedData = invitedDataFromJson(jsonString);

import 'dart:convert';

List<InvitedData> invitedDataFromJson(String str) => List<InvitedData>.from(json.decode(str).map((x) => InvitedData.fromJson(x)));

String invitedDataToJson(List<InvitedData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class InvitedData {
  InvitedData({
    required this.id,
    required this.firstname,
    required this.surname,
    required this.username,
    required this.dp,
  });

  int id;
  String firstname;
  String surname;
  String username;
  String dp;

  factory InvitedData.fromJson(Map<String, dynamic> json) => InvitedData(
    id: json["id"],
    firstname: json["firstname"],
    surname: json["surname"],
    username: json["username"],
    dp: json["dp"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstname": firstname,
    "surname": surname,
    "username": username,
    "dp": dp,
  };
}
